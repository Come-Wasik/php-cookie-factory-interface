<?php

namespace Nolikein\Cookie;

interface CookieFactoryInterface
{
    /**
     * Create a new cookie object
     * 
     * Note that this object is compatible with arguments of the native set_cookie function.
     * 
     * @param string $name The name of the cookie.
     * @param string $value The value of the cookie. Do not stock any important client data here.
     * @param \Datetime|int $expire The time when the cookie will be expired. If inject an int, it MUST be to a timestamp.
     * @param string $path The path on the server in which the cookie will be available on.
     * @param string $domain The (sub)domain that the cookie is available to.
     * @param bool $isSecure Indicates that the cookie should only be transmitted over a secure HTTPS connection from the client.
     * @param bool $isHttpOnly When TRUE the cookie will be made accessible only through the HTTP protocol.
     */
    public function createCookie(string $name, string $value = "", $expire = 0, string $path = "", string $domain = "", bool $isSecure = false, bool $isHttpOnly = false): CookieInterface;
}
